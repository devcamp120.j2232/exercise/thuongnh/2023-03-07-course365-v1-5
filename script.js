var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}


"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict"
    console.log("đã load xong thẻ body ");
    // b1 thu thâpj dữ liệu bỏ qua 
    // b2 validate data bỏ qua 
    // b3 call server
    getCallServerCoursesList(function (response) {
        console.log(response);
        // b4 hiển thị font end 
        // lọc trending và Popular   ( thịnh hành và phổ biến )
        var vObjectArrCourses = filterCourses(response);
        taoTheCardKhoaHoc(vObjectArrCourses.arrPopular, 0, 4, "#most-popular"); // Most popular - nhiều người học nhất
        taoTheCardKhoaHoc(vObjectArrCourses.arrTrending, 0, 4, "#div-card-trending"); //  Trending - đang lên (đang nổi)


    }
    );

    taoTheCardKhoaHoc(gCoursesDB.courses, 0, 4, "#Recommended-to-you"); // Recommended to you: đề xuất cho bạn
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// khai báo hàm ..
// truyền tham số .. object các khóa học . số bắt đầu , số kết thúc..  và ID của phần tử cần add vào
// đầu ra : các thẻ card được hiển thị .. các tham số được truyền vào thẻ card
function taoTheCardKhoaHoc(paramObject, paramBatDau, ParamKetThuc, paramIdDivCard) {
    // paramBatDau ParamKetThuc 
    for (var i = paramBatDau; i < ParamKetThuc; i++) {
        // tạo ra thẻ car html 
        var vCard = $(`<div class="col-sm-3">
        <div class="card">
          <img src="` + paramObject[i].coverImage + `" alt="" style="width: 100%;">
          <div class="card-body ">
            <b class="p-mo-ta">` + paramObject[i].courseName + `</b>
            <p> <i class="far fa-clock"></i> ` + paramObject[i].duration + ` </p>
            <p> <b>$` + paramObject[i].discountPrice + `</b> <span class="ganh-ngang-chu">$` + paramObject[i].price + `</span> </p>
          </div>
          <div class="card-footer ">
            <div class="row">
              <div class="col-sm-12">
                <img class="img-avarta-teacher" src="` + paramObject[i].teacherPhoto + `" alt="">
                <label>` + paramObject[i].teacherName + `</label>
                <i class="far fa-bookmark bookmark-css"></i>
              </div>
            </div>
          </div>
        </div>
      </div>`);
        // thêm thẻ vcard vào thể div có id="Recommended-to-you"
        $(vCard).appendTo(paramIdDivCard);
    }
}

// gọi server để lấy danh sách 
function getCallServerCoursesList(paramFunction) {
    $.ajax({
        type: "GET",
        url: "https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses",
        dataType: "json",
        success: paramFunction,
        error: function (paramError) {
            console.log("lỗi gọi server", paramError.status);
        }
    });
}

// input arrObject courses ( arr khoá học )
// output: đối tượng chứa hai mảng : khóa học thịnh hành . và khóa học phổ biến 
function filterCourses(paramObject) {
    // khai báo đối tượng cần trả về 
    var vObjectTwoCourses = {
        arrPopular: null,
        arrTrending: null,
    }
    // lọc khóa học trending 
    var filterTrending = paramObject.filter(function (str) {
        return str.isTrending == true;
    });
    // lọc khóa học popular (phổ biến )
    var filterPopular = paramObject.filter(function (str) {
        return str.isPopular == true;
    });
    // gán data vào đối tượng vObjectTwoCourses
    vObjectTwoCourses.arrPopular  = filterPopular;
    vObjectTwoCourses.arrTrending = filterTrending;
    console.log("Object", vObjectTwoCourses);
    // trả về OBject
    return vObjectTwoCourses;

}


